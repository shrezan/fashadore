package com.shrezan.fashadore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FashadoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(FashadoreApplication.class, args);
	}

}

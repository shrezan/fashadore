package com.shrezan.fashadore.sql;

import com.shrezan.fashadore.ex.FashadoreException;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class JDBCException extends FashadoreException {
    public JDBCException(String message) {
        super(message);
    }

    public JDBCException(Throwable throwable) {
        super(throwable);
    }
}
package com.shrezan.fashadore.sql;

import java.sql.Connection;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public interface ConnectionFactory {

    void start() throws JDBCException;

    DbInfo config();

    Connection getConnection() throws JDBCException;

    void shutdown();
}

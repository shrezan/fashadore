package com.shrezan.fashadore.sql;

import java.sql.Connection;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public abstract class AbstractConnectionFactory implements ConnectionFactory {

    private ConnectionPool connectionPool;

    public void start() throws JDBCException {
        DbInfo dbInfo = this.config();
        connectionPool = ConnectionPool.create(dbInfo);
    }

    public Connection getConnection() throws JDBCException {
        return this.connectionPool.getConnection();
    }

    public void shutdown() {
        this.connectionPool.destroy();
    }
}

package com.shrezan.fashadore.sql;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.stereotype.Repository;

import java.sql.*;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Repository
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class SqlDataSource {
    private final ConnectionFactory connectionFactory;

    public SqlDataSource(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public PreparedStatement prepareStatement(String query, Connection connection) throws JDBCException {
        if (connection == null) throw new JDBCException("Connection is null.");
        if (query == null || query.trim().length() == 0) throw new JDBCException("Query is null or empty.");
        try {
            //No need to update and we may iterate many times so we need scrollable
            return connection.prepareStatement(query,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException e) {
            throw new JDBCException(e);
        }
    }

    public Statement statement(Connection connection) throws JDBCException {
        if (connection == null) throw new JDBCException("Connection is null.");
        try {
            return connection.createStatement();
        } catch (SQLException e) {
            throw new JDBCException(e);
        }
    }

    public CallableStatement call(String query, Connection connection) throws JDBCException {
        if (connection == null) throw new JDBCException("Connection is null.");
        if (query == null || query.trim().length() == 0) throw new JDBCException("Query is null or empty.");
        try {
            return connection.prepareCall(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException e) {
            throw new JDBCException(e);
        }
    }

    public Connection getConnection() throws JDBCException, SQLException {
        Connection connection = this.connectionFactory.getConnection();
        connection.setAutoCommit(true);
        return connection;
    }

    public int rows(ResultSet resultSet) throws JDBCException {
        int i = 0;
        try {
            while (resultSet.next()) {
                i++;
            }
        } catch (SQLException e) {
            throw new JDBCException(e);
        }
        return i;
    }
}

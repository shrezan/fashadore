package com.shrezan.fashadore.cache;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public interface CacheManager<T extends Cache> {
    void start();

    default void start(int database) {
    }

    default void start(String host, int port, String password, int database) {
    }

    default void start(String host, int port, int database) {
    }

    default void start(String host, int port) {
    }

    T getCache();

    void stop();
}

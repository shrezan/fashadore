package com.shrezan.fashadore.cache;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.async.RedisAsyncCommands;
import com.shrezan.fashadore.redis.BinaryRedisCodec;
import com.shrezan.fashadore.redis.LettuceClient;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public final class RedisCacheManager implements CacheManager<Cache<byte[]>> {
    private RedisAsyncCommands<String, byte[]> COMMAND;
    private static final Logger logger = LoggerFactory.getLogger(RedisCacheManager.class);
    private static final Config CONFIG = ConfigFactory.load();

    @Override
    public void start() {
        String host = CONFIG.getString("redis.host");
        int port = CONFIG.getInt("redis.port");
        int database = CONFIG.getInt("redis.database");
        String password = CONFIG.getString("redis.password");
        this._start(host, port, password, database);
    }

    @Override
    public void start(int database) {
        String host = CONFIG.getString("redis.host");
        int port = CONFIG.getInt("redis.port");
        String password = CONFIG.getString("redis.password");
        this._start(host, port, password, database);
    }

    @Override
    public void start(String host, int port, String password, int database) {
        this._start(host, port, password, database);
    }

    private void _start(String host, int port, String password, int database) {
        if (null == COMMAND) {
            RedisURI redisURI = RedisURI.create(host, port);
            redisURI.setDatabase(database);
            if (null != password && password.trim().length() > 0) {
                redisURI.setPassword(password);
            }
            RedisClient client = RedisClient.create(redisURI);
            COMMAND = client.connect(new BinaryRedisCodec()).async();
            logger.debug("Started redis client for redis cache : {}/{}", redisURI.getHost(), database);
        }
    }

    @Override
    public void start(String host, int port, int database) {
        this._start(host, port, null, database);
    }

    @Override
    public void start(String host, int port) {
        int database = CONFIG.getInt("redis.database");
        this._start(host, port, null, database);
    }

    @Override
    public Cache<byte[]> getCache() {
        //TODO Redis connection life cycle should maintained.
        return new LettuceClient(COMMAND);
    }

    @Override
    public void stop() {
        COMMAND.close();
        logger.debug("Stopped redis client for redis cache.");
    }
}

package com.shrezan.fashadore.cache;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public interface Cache<T> {
    boolean set(String key, T value, int ttl);

    boolean set(String key, T value);

    T get(String key);

    boolean delete(String... key);
}

package com.shrezan.fashadore.api;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Component
@Order(1)
public class ResponseFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String contentType = request.getHeader("Content-Type");
        if (null != contentType) {

        }
        String accept = request.getHeader("Accept");
        if (MediaTypes.JSON.equalsIgnoreCase(accept)) {
            response.addHeader("Content-Type", MediaTypes.JSON);
        }
        filterChain.doFilter(new AddParamsToHeader(request), response);
    }

    public class AddParamsToHeader extends HttpServletRequestWrapper {
        public AddParamsToHeader(HttpServletRequest request) {
            super(request);
        }

        public String getHeader(String name) {
            String header = super.getHeader(name);
            if (name.equalsIgnoreCase("Content-Type")) {
                return (header != null) ? header : MediaTypes.JSON;
            } else {
                return header;
            }
        }

        public Enumeration getHeaderNames() {
            List<String> names = Collections.list(super.getHeaderNames());
            names.add("Content-Type");
            return Collections.enumeration(names);
        }
    }
}

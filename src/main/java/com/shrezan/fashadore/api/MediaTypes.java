package com.shrezan.fashadore.api;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class MediaTypes {
    public static final String JSON = "application/json";
    public static final String PROTOBUFF = "application/protobuf";
}

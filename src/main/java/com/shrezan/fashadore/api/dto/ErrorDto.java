package com.shrezan.fashadore.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorDto {
    private final String msg;
    private final boolean error;
    private final String errorCode = "INVALID";

    public ErrorDto() {
        this("Json parse error.");
    }

    public ErrorDto(String msg) {
        this.msg = msg;
        this.error = true;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public boolean isError() {
        return error;
    }
}

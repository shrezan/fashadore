package com.shrezan.fashadore.api.servicecall;

import com.shrezan.fashadore.entities.Authorization;
import com.shrezan.fashadore.entities.LoginRequest;
import com.shrezan.fashadore.entities.Response;
import com.shrezan.fashadore.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Service
public class AccountServiceCall {
    private final AccountService accountService;

    @Autowired
    public AccountServiceCall(final AccountService accountService) {
        this.accountService = accountService;
    }

    public Response login(LoginRequest loginRequest) {

        return accountService.login(loginRequest);
    }

    public Response logout(Authorization authorization) {
        return accountService.logout(authorization);
    }
}

package com.shrezan.fashadore.api.servicecall;

import com.shrezan.fashadore.entities.AddUser;
import com.shrezan.fashadore.entities.Authorization;
import com.shrezan.fashadore.entities.Response;
import com.shrezan.fashadore.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Service
public class UserServiceCall {
    private final UserService userService;

    @Autowired
    public UserServiceCall(final UserService userService) {
        this.userService = userService;
    }

    public Response addUser(AddUser addUser) {
        return userService.addUser(addUser.getUser(), addUser.getPassword());
    }

    public Response getMerchants(Authorization authorization) {
        return userService.getMerchants(authorization);
    }

    public Response getBuyers(Authorization authorization) {
        return userService.getBuyers(authorization);
    }

    public Response getUserById(Authorization authorization, String userId) {
        return userService.getUserById(authorization, userId);
    }

//    public Response getUserByEmail(Authorization authorization, String email) {
//        return userService.getUserByEmail(authorization, email);
//    }
}

package com.shrezan.fashadore.api.controllers;

import com.shrezan.fashadore.api.MediaTypes;
import com.shrezan.fashadore.api.servicecall.UserServiceCall;
import com.shrezan.fashadore.entities.AddUser;
import com.shrezan.fashadore.entities.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController extends BaseController {
    @Autowired
    private UserServiceCall userServiceCall;


    @PostMapping(value = "/user",
            produces = {MediaTypes.JSON, MediaTypes.PROTOBUFF},
            consumes = {MediaTypes.JSON, MediaTypes.PROTOBUFF})
    public ResponseEntity<Response> addUser(@RequestBody AddUser addUser) {
        return response(userServiceCall.addUser(addUser));
    }

    @GetMapping(value = "/merchants",
            produces = {MediaTypes.JSON, MediaTypes.PROTOBUFF})
    public ResponseEntity<Response> getMerchants(@RequestHeader(value = "Authorization") String authorization) {
        return response(userServiceCall.getMerchants(auth(authorization)));
    }

    @GetMapping(value = "/buyers",
            produces = {MediaTypes.JSON, MediaTypes.PROTOBUFF})
    public ResponseEntity<Response> getBuyers(@RequestHeader(value = "Authorization") String authorization) {
        return response(userServiceCall.getMerchants(auth(authorization)));
    }

    @GetMapping(value = "/user/{userId}",
            produces = {MediaTypes.JSON, MediaTypes.PROTOBUFF})
    public ResponseEntity<Response> getUserById(
            @RequestHeader(value = "Authorization") String authorization,
            @PathVariable("userId") final String userId) {
        return response(userServiceCall.getUserById(auth(authorization), userId));
    }

//    @GetMapping(value = "/user/email/{emailId}",
//            produces = {MediaTypes.JSON, MediaTypes.PROTOBUFF})
//    public ResponseEntity<Response> getUserByEmail(
//            @RequestHeader(value = "Authorization") String authorization,
//            @PathVariable("emailId") final String emailId) {
//        return response(userServiceCall.getUserByEmail(auth(authorization), emailId));
//    }
}

package com.shrezan.fashadore.api.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.shrezan.fashadore.api.MediaTypes;
import com.shrezan.fashadore.entities.Authorization;
import com.shrezan.fashadore.entities.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
class BaseController {
    private static final Logger logger = LoggerFactory.getLogger(BaseController.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final MultiValueMap JSON_HEADER = new LinkedMultiValueMap(1);

    BaseController() {
        JSON_HEADER.put("Content-Type", Lists.newArrayList(MediaTypes.JSON));
    }

    ResponseEntity<Response> response(Response message) {
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    ResponseEntity<Response> response(String accept, Response message) {
        if (MediaTypes.JSON.equalsIgnoreCase(accept)) {
            return new ResponseEntity<>(message, JSON_HEADER, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.OK);
        }
    }

    private <E> String serialize(E data) {
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            logger.error("Error in serializing.", e);
        }
        return null;
    }

    Authorization auth(String authorization) {
        Authorization authBuilder = new Authorization();
        authBuilder.setToken(authorization);
        return authBuilder;
    }
}

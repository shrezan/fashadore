package com.shrezan.fashadore.api.controllers;

import com.shrezan.fashadore.api.MediaTypes;
import com.shrezan.fashadore.api.servicecall.AccountServiceCall;
import com.shrezan.fashadore.entities.LoginRequest;
import com.shrezan.fashadore.entities.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class AccountController extends BaseController {
    @Autowired
    private AccountServiceCall accountServiceCall;

    @PostMapping(value = "/login",
            produces = {MediaTypes.JSON, MediaTypes.PROTOBUFF},
            consumes = {MediaTypes.JSON, MediaTypes.PROTOBUFF})
    public ResponseEntity<Response> login(@RequestBody LoginRequest loginRequest) {
        return response(accountServiceCall.login(loginRequest));
    }

    @GetMapping(value = "/logout",
            produces = {MediaTypes.JSON, MediaTypes.PROTOBUFF})
    public ResponseEntity<Response> logout(
            @RequestHeader(value = "Authorization") String authorization) {
        return response(accountServiceCall.logout(auth(authorization)));
    }
}

package com.shrezan.fashadore.services;

import com.shrezan.fashadore.entities.AuthResponse;
import com.shrezan.fashadore.entities.Authorization;
import com.shrezan.fashadore.entities.ErrorCode;
import com.shrezan.fashadore.entities.Response;
import com.shrezan.fashadore.ex.AuthException;
import com.shrezan.fashadore.ex.PermissionException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/

abstract class Service {
    private final AccountService accountService;

    @Autowired
    public Service(final AccountService accountService) {
        this.accountService = accountService;
    }

    public Response invalid(String msg) {
        return new Response(true, msg, ErrorCode.INVALID.name());
    }

    public Response failed(String msg) {
        return new Response(true, msg, ErrorCode.FAILED.name());
    }

    public Response duplicate(String msg) {
        return new Response(true, msg, ErrorCode.DUPLICATE.name());
    }

    public Response exception(String msg) {
        return new Response(true, msg, ErrorCode.EXCEPTION.name());
    }

    public Response notFound(String msg) {
        return new Response(true, msg, ErrorCode.NOT_FOUND.name());
    }

    public Response unAuthorized(String msg) {
        return new Response(true, msg, ErrorCode.UNAUTHORIZED.name());
    }

    public AuthResponse authorize(Authorization authorization, String permission)
            throws AuthException {
        authorization.setPermission(permission);
        Response response = this.accountService
                .authorize(authorization);
        if (null == response) {
            throw new AuthException();
        }
        if (response.isError()) {
            throw new AuthException();
        }
        if (!response.getAuthResponse().isGranted()) {
            throw new PermissionException();
        }
        return response.getAuthResponse();
    }

    public Response exception(Throwable throwable, final Logger logger) {
        Response response = new Response();
        if (throwable instanceof PermissionException) {
            response.setMsg("User don't have permission.");
            response.setErrorCode(ErrorCode.UNAUTHORIZED);
        } else if (throwable instanceof AuthException) {
            response.setMsg("Authorization failed.");
            response.setErrorCode(ErrorCode.UNAUTHORIZED);
        } else {
            response.setMsg(throwable.getMessage() != null ? throwable.getMessage() : "Server exception");
            response.setErrorCode(ErrorCode.EXCEPTION);
        }
        if (null != logger && !(throwable instanceof AuthException)) {
            logger.error("Exception in server", throwable);
        }
        return response;
    }
}

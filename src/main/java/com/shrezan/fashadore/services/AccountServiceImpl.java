package com.shrezan.fashadore.services;

import com.google.common.base.Charsets;
import com.shrezan.fashadore.entities.*;
import com.shrezan.fashadore.repo.AuthRepo;
import com.shrezan.fashadore.repo.UserRepo;
import com.shrezan.fashadore.utils.FashadoreDate;
import com.shrezan.fashadore.utils.FashadoreUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Service
public class AccountServiceImpl extends AccountService{
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);
    private final UserRepo userRepo;
    private final AuthRepo authRepo;

    @Autowired
    public AccountServiceImpl(final UserRepo userRepo,
                              final AuthRepo authRepo) {
        super(null);
        this.userRepo = userRepo;
        this.authRepo = authRepo;
    }

    @Override
    public Response login(LoginRequest loginRequest) {
        String username = loginRequest.getUsername();
        String password = loginRequest.getPassword();
        if (null == username || username.trim().length() <= 0) {
            return invalid("Invalid username or password.");
        }
        if (null == password) {
            return invalid("Invalid username or password.");
        }
        try {
            LoginResult loginResult = userRepo.login(username);
            if (null == loginResult) {
                return invalid("Invalid username or password.");
            } else {
                if (FashadoreUtils.checkPassword(password, loginResult.password)) {
                    String authToken = createSession(loginResult.user, loginRequest);
                    if (null == authToken) {
                        logger.error("Could not create session after login.");
                        return failed("Could not create session after login.");
                    } else {
                        LoginResponse loginResponse = new LoginResponse(authToken, loginResult.user);
                        Response response = new Response();
                        response.setError(false);
                        response.setLoginResponse(loginResponse);
                        return response;
                    }
                } else {
                    return invalid("Invalid username or password.");
                }
            }
        } catch (Exception ex) {
            return exception(ex, logger);
        }
    }

    public Response authorize(final Authorization authorization) {
        try {
            final Session session = authorize(authorization.getToken());
            if (null == session) {

                return unAuthorized("Authorization failed.");
            }
            return buildAuthResponse(session, this.permission(session, authorization.getPermission()));
        } catch (Exception ex) {
            return exception(ex, logger);
        }
    }


    public Response logout(Authorization authorization) {
        try {
            final Session session = authorize(authorization.getToken());
            if (null != session) {
                authRepo.remove(session);
            }
            Response response = new Response();
            response.setError(false);
            response.setSuccess(true);
            response.setMsg("Logged out successfully.");

            return response;
        } catch (Exception e) {
            return exception(e, logger);
        }
    }

    private Response buildAuthResponse(Session session, boolean granted) {
        AuthResponse authResponse = new AuthResponse(granted, session);
        Response response = new Response();
        response.setAuthResponse(authResponse);
        return response;
    }

    private boolean permission(Session session, String permission) {
        if (null == permission) {
            return false;
        }
        switch (permission) {
            case "user.add":
            case "user.update":
                return true;
        }
        return true;
    }

    private Session authorize(final String authorization) {
        if (null == authorization) return null;
        String[] authParts = authorization.split("\\.");
        if (authParts.length == 2) {
            String payload = FashadoreUtils.decodeBase64(authParts[0]);
            String signature = authParts[1];
            String[] payloadParts = payload.split("\\.");
            if (payloadParts.length == 2) {
                String userId = payloadParts[0];
                String sessionId = payloadParts[1];
                Jwt session = authRepo.check(userId, sessionId);
                if (null != session) {
                    String checkSignature = FashadoreUtils.hmacSha512(session.getSignatureSecret(),
                            FashadoreUtils.getBytes(payload));
                    if (check(signature, checkSignature)) {
                        return session.getSession();
                    }
                }
            }
        }
        return null;
    }

    private boolean check(String one, String two) {
        try {
            byte[] oneBytes = one.getBytes(Charsets.UTF_8);
            byte[] twoBytes = two.getBytes(Charsets.UTF_8);
            if (oneBytes.length != twoBytes.length) {
                return false;
            }
            byte ret = 0;
            for (int i = 0; i < twoBytes.length; i++) {
                ret |= oneBytes[i] ^ twoBytes[i];
            }
            return ret == 0;
        } catch (Exception e) {
            logger.error("Error while getting bytes.", e);
        }
        return false;
    }

    private String createSession(final User user, final LoginRequest login) {
        final String sessionId = FashadoreUtils.uuid();
        final String userId = user.getUserId();
        final DeviceType deviceType = login.getDeviceType();
        final String deviceInfo = login.getDeviceInfo();
        final String deviceId = login.getDeviceId();
        final String ip = login.getIp();
        final long timestamp = FashadoreDate.timestamp();
        final boolean active = true;
        final String pushToken = login.getPushToken();

        final Session session = new Session(sessionId, userId, deviceType, deviceInfo, deviceId, ip,
                timestamp, active, user, pushToken);
        String secret = FashadoreUtils.uuid();

        Jwt jwt = new Jwt(secret, session);
        if (authRepo.save(jwt)) {
            String payload = String.format("%s.%s", session.getUser().getUserId(), session.getSessionId());
            final String signature = FashadoreUtils.hmacSha512(secret, FashadoreUtils.getBytes(payload));
            return String.format("%s.%s", FashadoreUtils.encodeBase64(payload), signature);
        } else {
            return null;
        }
    }
}

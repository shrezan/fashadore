package com.shrezan.fashadore.services;

import com.shrezan.fashadore.entities.Authorization;
import com.shrezan.fashadore.entities.Response;
import com.shrezan.fashadore.entities.User;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/

public abstract class UserService extends Service {
    public UserService(AccountService accountService) {
        super(accountService);
    }

    public abstract Response addUser(User user, String password);

    public abstract Response getMerchants(Authorization authorization);

    public abstract Response getBuyers(Authorization authorization);

    public abstract Response getUserById(Authorization authorization, String userId);

    public abstract Response getUserByEmail(Authorization authorization, String userId);
}

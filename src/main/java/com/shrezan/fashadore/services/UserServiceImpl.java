package com.shrezan.fashadore.services;

import com.shrezan.fashadore.entities.Authorization;
import com.shrezan.fashadore.entities.Response;
import com.shrezan.fashadore.entities.User;
import com.shrezan.fashadore.repo.UserRepo;
import com.shrezan.fashadore.utils.FashadoreDate;
import com.shrezan.fashadore.utils.FashadoreUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Service
public class UserServiceImpl extends UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepo userRepo;

    @Autowired
    public UserServiceImpl(AccountService accountService, UserRepo userRepo) {
        super(accountService);
        this.userRepo = userRepo;
    }

    @Override
    public Response addUser(User user, String password) {
        try {
            if (StringUtils.isBlank(user.getEmail())) {
                logger.debug("Email not specified");
                return invalid("Email not specified");
            }
            if (StringUtils.isBlank(password)) {
                logger.debug("Password not specified");
                return invalid("Password not specified");
            }
            if (user.getUserType() == User.UserType.UNKNOWN_USERTYPE) {
                logger.debug("User Type not specified");
                return invalid("User Type not specified");
            }
            if (userRepo.getUserByEmail(user.getEmail()) != null) {
                logger.debug("User already exists.");
                return duplicate("User already exists.");
            }
            user.setUserId(FashadoreUtils.uuid());
            user.setCreatedAt(FashadoreDate.timestamp());
            if (userRepo.addUser(user, password)) {
                Response response = new Response();
                response.setUser(user);
                return response;
            }
            logger.debug("Failed to add user.");
            return failed("Failed to add user.");
        } catch (Exception ex) {
            logger.error("Error while adding user.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public Response getMerchants(Authorization authorization) {
        try {
            this.authorize(authorization, "user.get");
            List<User> users = userRepo.getMerchants();
            return getUsers(users);
        } catch (Exception ex) {
            logger.error("Error while getting merchants details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public Response getBuyers(Authorization authorization) {
        try {
            this.authorize(authorization, "user.get");
            List<User> users = userRepo.getBuyers();
            return getUsers(users);
        } catch (Exception ex) {
            logger.error("Error while getting companies details.", ex);
            return exception(ex, logger);
        }
    }

    private Response getUsers(List<User> users) {
        if(users.isEmpty()) {
            logger.debug("No users found.");
            return failed("No users found.");
        } else {
            Response response = new Response();
            response.setUsers(users);
            return response;
        }
    }

    @Override
    public Response getUserById(Authorization authorization,
                                String userId) {
        try {
            this.authorize(authorization, "user.get");
            if (null == userId) {
                logger.debug("User Id not provided");
                return invalid("User Id not provided");
            }
            User user = userRepo.getUserById(userId);
            Response response = new Response();
            response.setUser(user);
            return response;
        } catch (Exception ex) {
            logger.error("Error while getting user details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public Response getUserByEmail(Authorization authorization, String email) {
        try {
            this.authorize(authorization, "user.get");
            if (null == email) {
                logger.debug("Email not provided");
                return invalid("Email not provided");
            }
            User user = userRepo.getUserByEmail(email);
            Response response = new Response();
            response.setUser(user);
            return response;
        } catch (Exception ex) {
            logger.error("Error while getting user details.", ex);
            return exception(ex, logger);
        }
    }
}

package com.shrezan.fashadore.services;

import com.shrezan.fashadore.entities.Authorization;
import com.shrezan.fashadore.entities.LoginRequest;
import com.shrezan.fashadore.entities.Response;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public abstract class AccountService extends Service {
    public AccountService(AccountService accountService) {
        super(accountService);
    }

    public abstract Response login(LoginRequest loginRequest);

    public abstract Response authorize(final Authorization authorization);

    public abstract Response logout(Authorization authorization);

}

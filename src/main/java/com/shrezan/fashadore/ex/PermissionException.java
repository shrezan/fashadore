package com.shrezan.fashadore.ex;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class PermissionException extends AuthException {

    public PermissionException() {
        super("Permission denied.");
    }
}

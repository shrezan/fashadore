package com.shrezan.fashadore.ex;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class AuthException extends FashadoreException {
    public AuthException() {
        super("Authorization failed.");
    }

    public AuthException(String msg) {
        super(msg);
    }
}

package com.shrezan.fashadore.ex;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class FashadoreException extends Exception {
    public FashadoreException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public FashadoreException(String msg) {
        super(msg);
    }

    public FashadoreException(Throwable throwable) {
        super(throwable);
    }
}

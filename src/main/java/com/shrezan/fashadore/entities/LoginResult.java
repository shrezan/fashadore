package com.shrezan.fashadore.entities;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public final class LoginResult {
    public final String password;
    public final User user;

    public LoginResult(String password, User user) {
        this.password = password;
        this.user = user;
    }
}

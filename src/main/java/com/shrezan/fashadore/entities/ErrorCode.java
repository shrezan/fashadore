package com.shrezan.fashadore.entities;

public enum ErrorCode {
    INVALID,
    FAILED,
    DUPLICATE,
    EXCEPTION,
    NOT_FOUND,
    UNAUTHORIZED
}

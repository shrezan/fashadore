package com.shrezan.fashadore.entities;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public enum DeviceType {
    UNKNOWN_DEVICE,
    ANDROID,
    IOS,
    WEB,
    IOS_WEB,
    ANDROID_WEB
}

package com.shrezan.fashadore.entities;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class AuthResponse {
    private boolean granted;
    private Session session;

    public AuthResponse(boolean granted, Session session) {
        this.granted = granted;
        this.session = session;
    }

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}


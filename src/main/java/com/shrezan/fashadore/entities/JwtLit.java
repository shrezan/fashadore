package com.shrezan.fashadore.entities;

import com.shrezan.fashadore.repo.AuthRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class JwtLit {
    private static final Logger logger = LoggerFactory.getLogger(JwtLit.class);

    public static JwtLit parseFrom(byte[] data) {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is;
        try {
            is = new ObjectInputStream(in);
            return (JwtLit) is.readObject();
        } catch (IOException | ClassNotFoundException e) {
            logger.error("Error while converting to byte array.");
            return null;
        }
    }

    private List<Jwt> jwts;

    public List<Jwt> getJwts() {
        return jwts;
    }

    public void setJwts(List<Jwt> jwts) {
        this.jwts = jwts;
    }

    public byte[] toByteArray() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(bos);
            oos.writeObject(this.jwts);
            oos.flush();
            return bos.toByteArray();
        } catch (IOException e) {
            logger.error("Error while converting to byte array.");
            return new byte[0];
        }
    }
}

package com.shrezan.fashadore.entities;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class Session {
    private String sessionId;
    private String userId;
    private DeviceType deviceType;
    private String deviceInfo;
    private String deviceId;
    private String ip;
    private long timestamp;
    private boolean active;
    private User user;
    private String pushToken;

    public Session(String sessionId, String userId, DeviceType deviceType, String deviceInfo, String deviceId,
                   String ip, long timestamp, boolean active, User user, String pushToken) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.deviceType = deviceType;
        this.deviceInfo = deviceInfo;
        this.deviceId = deviceId;
        this.ip = ip;
        this.timestamp = timestamp;
        this.active = active;
        this.user = user;
        this.pushToken = pushToken;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }
}

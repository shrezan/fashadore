package com.shrezan.fashadore.repo;

import com.shrezan.fashadore.entities.LoginResult;
import com.shrezan.fashadore.entities.User;
import com.shrezan.fashadore.ex.FashadoreException;
import com.shrezan.fashadore.sql.SqlDataSource;

import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public abstract class UserRepo extends SqlRepo {
    protected UserRepo(SqlDataSource dataSource) {
        super(dataSource);
    }

    public abstract LoginResult login(String email) throws FashadoreException;

    public abstract boolean addUser(User user, String password);

    public abstract List<User> getMerchants() throws FashadoreException;

    public abstract List<User> getBuyers() throws FashadoreException;

    public abstract User getUserById(String userId) throws FashadoreException;

    public abstract User getUserByEmail(String email) throws FashadoreException;
}

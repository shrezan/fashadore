package com.shrezan.fashadore.repo;

import com.shrezan.fashadore.cache.Cache;
import com.shrezan.fashadore.entities.Jwt;
import com.shrezan.fashadore.entities.JwtLit;
import com.shrezan.fashadore.entities.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Repository
public class AuthRepo {
    private static final Logger logger = LoggerFactory.getLogger(AuthRepo.class);
    private static final Config CONFIG = ConfigFactory.load();
    private static final int DEFAULT_SESSION_TIMEOUT = CONFIG.getInt("session.timeout");
    @Autowired
    private final Cache<byte[]> cache;

    public AuthRepo(final Cache<byte[]> cache) {
        this.cache = cache;
    }

    public boolean remove(final Session session) {
        JwtLit holder = get(session.getUserId());
        if (null == holder || holder.getJwts().isEmpty()) return true;
        List<Jwt> sessions = holder.getJwts().stream()
                .filter(sess -> !session.getSessionId().equalsIgnoreCase(sess.getSession().getSessionId()))
                .collect(Collectors.toList());
        JwtLit jwtLit = new JwtLit();
        jwtLit.setJwts(sessions);
        return this.set(session.getUserId(), jwtLit);
    }

    public List<Jwt> getSessions(final String userId) {
        JwtLit holder = get(userId);
        if (null == holder || holder.getJwts().isEmpty()) return Collections.emptyList();
        return holder.getJwts();
    }

    public Jwt check(final String userId, final String sessionId) {
        JwtLit holder = get(userId);
        if (null == holder || holder.getJwts().isEmpty()) return null;
        for (Jwt session : holder.getJwts()) {
            if (session.getSession().getSessionId().equalsIgnoreCase(sessionId)) {
                return session;
            }
        }
        return null;
    }

    private JwtLit get(final String userId) {
        try {
            byte[] content = cache.get(userId);
            JwtLit holder;
            if (null == content || content.length == 0) {
                List<Jwt> sessions = new LinkedList<>();
                holder = new JwtLit();
                holder.setJwts(sessions);
            } else {
                holder = JwtLit.parseFrom(content);
            }

            return holder;
        } catch (Exception e) {
            logger.error("Exception while de serializing session", e);
        }
        return null;
    }

    private boolean set(String userId, JwtLit holder) {
        return cache.set(userId, holder.toByteArray());
    }

    public boolean update(final Jwt session) {
        JwtLit holder = get(session.getSession().getUserId());
        if (null == holder || holder.getJwts().isEmpty()) return true;
        List<Jwt> sessions = holder.getJwts().stream()
                .filter(sess -> !session.getSession().getSessionId()
                        .equalsIgnoreCase(sess.getSession().getSessionId()))
                .collect(Collectors.toList());
        sessions.add(session);
        JwtLit jwtLit = new JwtLit();
        jwtLit.setJwts(sessions);
        return this.set(session.getSession().getUserId(), jwtLit);
    }

    public boolean save(final Jwt session) {
        JwtLit holder = get(session.getSession().getUserId());
        if (null == holder) return false;
        List<Jwt> sessions = new LinkedList<>(holder.getJwts());
        sessions.add(session);
        JwtLit sessionHolder = new JwtLit();
        sessionHolder.setJwts(sessions);
        return this.set(session.getSession().getUserId(), sessionHolder);
    }
}
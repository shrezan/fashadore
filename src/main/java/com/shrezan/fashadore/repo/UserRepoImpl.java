package com.shrezan.fashadore.repo;

import com.shrezan.fashadore.entities.LoginResult;
import com.shrezan.fashadore.entities.User;
import com.shrezan.fashadore.ex.FashadoreException;
import com.shrezan.fashadore.sql.JDBCException;
import com.shrezan.fashadore.sql.SqlDataSource;
import com.shrezan.fashadore.utils.FashadoreUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Repository
public class UserRepoImpl extends UserRepo {
    private static final Logger logger = LoggerFactory.getLogger(UserRepoImpl.class);

    private static final String USER_TABLE = "user";
    private static final String USER_COLS = "user_id, user_type, full_name, card_no, email, address, phone, " +
            "created_at, updated_at";

    private static final String QUERY = "Query : {}";
    private static final String SELECT = "SELECT ";
    private static final String FROM = " FROM ";

    @Autowired
    public UserRepoImpl(SqlDataSource dataSource) {
        super(dataSource);
    }

    @Override
    public LoginResult login(String email) throws FashadoreException {
        String query = SELECT + USER_COLS + ", password FROM " + USER_TABLE + " WHERE email = ? AND is_deleted = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, email);
            statement.setBoolean(2, false);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = extractUser(resultSet);
                String password = resultSet.getString("password");
                return new LoginResult(password, user);
            } else {
                return null;
            }
        } catch (SQLException | JDBCException ex) {
            throw new FashadoreException("Error while getting user", ex);
        } finally {
            close(resultSet);
            close(statement);
            close(connection);
        }
    }

    @Override
    public boolean addUser(User user, String password) {
        String query = "INSERT INTO " + USER_TABLE + " (user_id, user_type, full_name, card_no, email, address, " +
                "password, phone, is_deleted, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, user.getUserId());
            statement.setString(2, user.getUserType().name());
            statement.setString(3, user.getName());
            statement.setLong(4, user.getCardNo());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getAddress());
            statement.setString(7, FashadoreUtils.passwordHash(password));
            statement.setString(8, user.getPhone());
            statement.setBoolean(9, false);
            statement.setLong(10, user.getCreatedAt());
            return statement.executeUpdate() == 1;
        } catch (SQLException | JDBCException ex) {
            logger.error("Error while adding user", ex);
            return false;
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public List<User> getMerchants() throws FashadoreException {
        String query = SELECT + USER_COLS + FROM + USER_TABLE + " WHERE user_type = ? AND is_deleted = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, User.UserType.MERCHANT.name());
            statement.setBoolean(2, false);
            ResultSet resultSet = statement.executeQuery();

            return getResults(resultSet, this::extractUser);
        } catch (SQLException | JDBCException ex) {
            throw new FashadoreException("Error while getting merchants", ex);
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public List<User> getBuyers() throws FashadoreException {
        String query = SELECT + USER_COLS + FROM + USER_TABLE + " WHERE user_type = ? AND is_deleted = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, User.UserType.BUYER.name());
            statement.setBoolean(2, false);
            ResultSet resultSet = statement.executeQuery();

            return getResults(resultSet, this::extractUser);
        } catch (SQLException | JDBCException ex) {
            throw new FashadoreException("Error while getting buyers", ex);
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public User getUserById(String userId) throws FashadoreException {
        String query = SELECT + USER_COLS + FROM + USER_TABLE + " WHERE user_id = ? AND is_deleted = ?";
        return getUser(userId, query);
    }

    @Override
    public User getUserByEmail(String email) throws FashadoreException {
        String query = SELECT + USER_COLS + FROM + USER_TABLE + " WHERE email = ? AND is_deleted = ?";
        return getUser(email, query);
    }

    private User getUser(String factor, String query) throws FashadoreException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, factor);
            statement.setBoolean(2, false);
            ResultSet resultSet = statement.executeQuery();
            return getResult(resultSet, this::extractUser);
        } catch (SQLException | JDBCException ex) {
            throw new FashadoreException("Error while getting user.", ex);
        } finally {
            close(statement);
            close(connection);
        }
    }

    private User extractUser(ResultSet resultSet) {
        try {
            User user = new User();
            user.setUserId(resultSet.getString("user_id"));
            user.setUserType(User.UserType.valueOf(resultSet.getString("user_type")));
            user.setName(resultSet.getString("full_name"));
            user.setEmail(resultSet.getString("email"));
            user.setAddress(resultSet.getString("address"));
            user.setPhone(resultSet.getString("phone"));
            user.setCardNo(resultSet.getLong("card_no"));
            user.setCreatedAt(resultSet.getLong("created_at"));
            user.setUpdatedAt(resultSet.getLong("updated_at"));
            return user;
        } catch (SQLException ex) {
            logger.error("Error while getting user resultset.", ex);
            return null;
        }
    }
}

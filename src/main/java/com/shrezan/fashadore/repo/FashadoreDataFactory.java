package com.shrezan.fashadore.repo;

import com.shrezan.fashadore.sql.AbstractConnectionFactory;
import com.shrezan.fashadore.sql.DbInfo;
import com.shrezan.fashadore.sql.JDBCException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Configuration
public class FashadoreDataFactory extends AbstractConnectionFactory {
    private static final Logger logger = LoggerFactory.getLogger(FashadoreDataFactory.class);
    private static final Config config = ConfigFactory.load();
    private static final FashadoreDataFactory INSTANCE = new FashadoreDataFactory();

    public FashadoreDataFactory() {
        try {
            this.start();
        } catch (JDBCException e) {
            logger.error("Error while initializing db connection.", e);
        }
    }

    public static FashadoreDataFactory getInstance() {
        if(INSTANCE == null) {
            return new FashadoreDataFactory();
        }
        return INSTANCE;
    }

    @Override
    public DbInfo config() {
        DbInfo dbInfo = new DbInfo();
        dbInfo.setValidationQuery(config.getString("fashadore.db.validation.query"));
        dbInfo.setPassword(config.getString("fashadore.db.password"));
        dbInfo.setUsername(config.getString("fashadore.db.user"));
        dbInfo.setDriver(config.getString("fashadore.db.driver"));
        dbInfo.setDatabaseName(config.getString("fashadore.db.name"));
        String url;
        try {
            url = config.getString("fashadore.db.url");
        } catch (ConfigException.Missing ex) {
            url = String.format("jdbc:mysql://%s:%d/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC",
                    config.getString("fashadore.db.host"),
                    config.getInt("fashadore.db.port"),
                    dbInfo.getDatabaseName());
        }
        dbInfo.setUrl(url);
        logger.debug("fashadore DB URL : {}", dbInfo.getUrl());
        return dbInfo;
    }
}

package com.shrezan.fashadore;

import com.shrezan.fashadore.cache.Cache;
import com.shrezan.fashadore.cache.RedisCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
@Component
public class CacheBean {
    @Bean
    public Cache<byte[]> init() {
        RedisCacheManager cacheManager = new RedisCacheManager();
        cacheManager.start();
        return cacheManager.getCache();
    }
}
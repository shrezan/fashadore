package com.shrezan.fashadore.redis;

import com.lambdaworks.redis.RedisFuture;
import com.lambdaworks.redis.SetArgs;
import com.lambdaworks.redis.api.async.RedisAsyncCommands;
import com.shrezan.fashadore.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-11-08
 **/
public class LettuceClient implements Cache<byte[]> {
    private final RedisAsyncCommands<String, byte[]> command;
    private static final Logger logger = LoggerFactory.getLogger(LettuceClient.class);

    public LettuceClient(final RedisAsyncCommands<String, byte[]> command) {
        this.command = command;
    }

    @Override
    public boolean set(String key, byte[] value, int ttl) {
        command.set(key, value, SetArgs.Builder.ex(ttl));
        return true;
    }

    @Override
    public boolean set(String key, byte[] value) {
        command.set(key, value);
        return true;
    }

    @Override
    public byte[] get(String key) {
        RedisFuture<byte[]> data = command.get(key);
        try {
            return data.get();
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Error while getting value from redis", e);
        }
        return new byte[0];
    }

    @Override
    public boolean delete(String... key) {
        command.del(key);
        return true;
    }
}

package com.shrezan.fashadore.utils;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.shrezan.fashadore.ex.FashadoreException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Dipak Malla
 * Email dpakmalla@gmail.com
 * Created on 4/4/19.
 */
public final class FashadoreUtils {
    private static final Logger logger = LoggerFactory.getLogger(FashadoreUtils.class);
    private static final Charset charset = Charsets.UTF_8;

    private FashadoreUtils() {
    }

    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String random30String() {
        return randomString(30);
    }

    public static String randomString(int count) {
        return RandomStringUtils.randomAlphanumeric(count);
    }

    public static int sixDigitRandomNumber() {
        Random random = new Random();
        int n = random.nextInt(999999);
        if (n >= 100000) return n;
        else return n + 100000;
    }

    public static String passwordHash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public static boolean checkPassword(String rawPassword, String hashedPassword) {
        return BCrypt.checkpw(rawPassword, hashedPassword);
    }

    public static String murmur3_128(String data) {
        byte[] bytes = getBytes(data);
        if (null == bytes) throw new IllegalArgumentException("Could not convert data into bytes.");
        return Hashing.murmur3_128().hashBytes(bytes).toString();
    }

    public static String sha512(String data) {
        byte[] bytes = getBytes(data);
        if (null == bytes) throw new IllegalArgumentException("Could not convert data into bytes.");
        return Hashing.sha512().hashBytes(bytes).toString();
    }

    public static String sha256(byte[] data) {
        return Hashing.sha256().hashBytes(data).toString();
    }

    public static String sha256(String data) {
        byte[] bytes = getBytes(data);
        if (null == bytes) throw new IllegalArgumentException("Could not convert data into bytes.");
        return sha256(bytes);
    }

    public static String hmacSha512(String secret, byte[] data) {
        byte[] key = getBytes(secret);
        if (null == key) throw new IllegalArgumentException("Could not convert key into bytes.");
        return Hashing.hmacSha512(key).hashBytes(data).toString();
    }

    public static String encodeBase64(String text) {
        if (null == text) {
            return null;
        }
        return java.util.Base64.getEncoder().encodeToString(text.getBytes());
    }

    public static String encodeBase64(byte[] text) {
        if (null == text) {
            return null;
        }
        return java.util.Base64.getEncoder().encodeToString(text);
    }

    public static String decodeBase64(String base64) {
        return new String(java.util.Base64.getDecoder().decode(base64));
    }

    public static byte[] decodeBase64AsBytes(String base64) {
        return java.util.Base64.getDecoder().decode(base64);
    }

    public static byte[] decodeBase64AsBytes(byte[] base64) {
        return java.util.Base64.getDecoder().decode(base64);
    }

    public static byte[] getBytes(String value) {
        CharsetEncoder encoder = charset.newEncoder();
        try {
            ByteBuffer buffer = encoder.encode(CharBuffer.wrap(value));
            byte[] bytes = new byte[buffer.limit()];
            buffer.get(bytes, 0, bytes.length);
            return bytes;
        } catch (CharacterCodingException e) {
            logger.error("Error while encoding", e);
        }
        return null;
    }

    public static String replaceNoneWord(String id) {
        return id.replaceAll("\\W", "");
    }

    public static IdType checkIdType(String id) throws FashadoreException {
        if (StringUtils.isBlank(id) || id.trim().length() < 5) {
            throw new FashadoreException("Invalid id.");
        }
        id = id.toUpperCase();
        try {
            String firstPart = id.substring(0, 4);
            int number = Integer.parseInt(firstPart);
            //Its either manual chit no. or driver
            if (number >= 4500) {
                return IdType.MANUAL_CHIT;
            } else if (number <= 1000) {
                return IdType.DRIVER_LICENSE;
            } else {
                return IdType.VEHICLE_NO;
            }
        } catch (Exception ex) {
            if (id.matches("^[A-Z][A-Z][0-9]+")) {
                return IdType.APP_CHIT;
            } else {
                return IdType.VEHICLE_NO;
            }
        }
    }

    public enum IdType {
        DRIVER_LICENSE, MANUAL_CHIT, APP_CHIT, VEHICLE_NO
    }
}

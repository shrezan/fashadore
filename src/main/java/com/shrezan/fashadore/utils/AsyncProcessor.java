package com.shrezan.fashadore.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class AsyncProcessor {
    private static final ExecutorService executorService = Executors.newFixedThreadPool(2);

    private AsyncProcessor() {
    }

    public static ExecutorService getService() {
        return executorService;
    }

    public static void shutdown() {
        executorService.shutdown();
    }
}

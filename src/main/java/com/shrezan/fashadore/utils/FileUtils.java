package com.shrezan.fashadore.utils;

import com.google.common.base.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

/**
 * Created by Dipak Malla
 * Email dpakmalla@gmail.com
 * Created on 5/16/18.
 */
public class FileUtils {
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);
    private static final Charset charset = Charsets.UTF_8;

    private FileUtils() {
    }

    public static byte[] toBytes(InputStream stream) throws IOException {
        return toBytes(stream, 4096);
    }

    public static String toString(byte[] bytes) {
        CharsetDecoder decoder = charset.newDecoder();
        try {
            CharBuffer charBuffer = decoder.decode(ByteBuffer.wrap(bytes));
            return charBuffer.toString();
        } catch (CharacterCodingException e) {
            logger.error("Error while decoding", e);
        }
        return null;
    }

    public static byte[] getBytes(String value) {
        CharsetEncoder encoder = charset.newEncoder();
        try {
            ByteBuffer buffer = encoder.encode(CharBuffer.wrap(value));
            byte[] bytes = new byte[buffer.limit()];
            buffer.get(bytes, 0, bytes.length);
            return bytes;
        } catch (CharacterCodingException e) {
            logger.error("Error while encoding", e);
        }
        return null;
    }

    public static boolean delete(File file) {
        if (null == file) {
            return false;
        }
        try {
            return file.delete();
        } catch (Throwable ignore) {
        }
        return false;
    }

    public static boolean checkIfFileExist(String path) {
        if (null == path) {
            return false;
        }
        File file = new File(path);
        return file.exists();
    }

    public static boolean delete(String path) {
        if (null == path) {
            return false;
        }
        return delete(new File(path));
    }

    public static String getFileNameFromPath(String path) {
        if (null == path) {
            return null;
        }
        File file = new File(path);
        return file.getName();
    }

    public static String getExtensionFromFileName(String fileName) {
        if (null == fileName) {
            return null;
        }
        String[] parts = fileName.split("\\.");
        if (parts.length <= 1) {
            return null;
        } else {
            return parts[parts.length - 1];
        }
    }

    public static boolean isImage(String mime) {
        if (null == mime) {
            return false;
        }
        switch (mime) {
            case "image/gif":
            case "image/x-icon":
            case "image/jpeg":
            case "image/jpg":
            case "image/png":
            case "image/svg+xml":
            case "image/tiff":
                return true;
        }
        return false;
    }

    public static String getExtension(String mime) {
        if (null == mime) {
            return null;
        }
        switch (mime) {
            case "image/gif":
                return "gif";
            case "image/x-icon":
                return "icon";
            case "image/jpeg":
            case "image/jpg":
                return "jpg";
            case "image/png":
                return "png";
            case "image/svg+xml":
                return "svg";
            case "image/tiff":
                return "tiff";
        }
        return null;
    }

    /*public static String getExtension(String mime){
        return null;
    }*/
    public static boolean isVideo(String mime) {
        if (null == mime) {
            return false;
        }
        switch (mime) {
            case "video/x-msvideo":
            case "video/mpeg":
            case "video/ogg":
            case "video/webm":
            case "video/3gpp":
            case "video/3gpp2":
            case "video/mp4":
            case "application/x-mpegURL":
            case "video/x-flv":
            case "video/MP2T":
            case "video/quicktime":
            case "video/video/x-ms-wmv":
                return true;
        }
        return false;
    }

    public static byte[] toBytes(InputStream stream, int bufferSize) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[bufferSize];
            int read;
            while ((read = stream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, read);
            }
            byteArrayOutputStream.flush();
            return byteArrayOutputStream.toByteArray();
        } finally {
            close(byteArrayOutputStream);
        }
    }

    public static InputStream toInputStream(byte[] bytes) {
        return new ByteArrayInputStream(bytes);
    }

    public static void reset(InputStream stream) {
        try {
            stream.reset();
        } catch (IOException ignore) {
        }
    }

    public static void close(InputStream stream) {
        if (null != stream) {
            try {
                stream.close();
            } catch (IOException ignore) {
            }
        }
    }

    public static void close(Reader reader) {
        if (null != reader) {
            try {
                reader.close();
            } catch (IOException ignore) {
            }
        }
    }

    public static void close(OutputStream stream) {
        if (null != stream) {
            try {
                stream.close();
            } catch (IOException ignore) {
            }
        }
    }
}

package com.shrezan.fashadore.utils.http;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dipak Malla
 * Email dpakmalla@gmail.com
 * Created on 11/21/18.
 */
public class Http {
    public enum ContentType {
        JSON, XML, TEXT
    }

    private static final String userAgent = "Mozilla/5.0";
    private String payload;
    private Map<String, String> headers;
    private Map<String, String> params;
    private ContentType contentType;

    private Http(Map<String, String> headers, Map<String, String> params, String payload, ContentType contentType) {
        this.headers = headers;
        this.params = params;
        this.payload = payload;
        this.contentType = contentType;
    }

    public static class Builder {
        private String payload;
        private Map<String, String> headers;
        private Map<String, String> params;
        private ContentType contentType;

        public Builder headers(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        public Builder params(Map<String, String> params) {
            this.params = params;
            return this;
        }

        public Builder payload(String payload, ContentType contentType) {
            this.payload = payload;
            this.contentType = contentType;
            return this;
        }

        public Builder payload(String payload) {
            this.payload = payload;
            this.contentType = ContentType.JSON;
            return this;
        }

        public Http build() {
            return new Http(headers, params, payload, contentType);
        }
    }

    private Response send(HttpEntityEnclosingRequestBase requestBase) throws HttpException {
        requestBase.setHeader("User-Agent", Http.userAgent);
        if (headers != null) {
            for (Map.Entry<String, String> kv : headers.entrySet()) {
                requestBase.setHeader(kv.getKey(), kv.getValue());
            }
        }
        try {
            if (payload != null) {
                org.apache.http.entity.ContentType contentType = org.apache.http.entity.ContentType.APPLICATION_JSON;
                String cType = "application/json";
                if (this.contentType != null) {
                    switch (this.contentType) {
                        case XML:
                            contentType = org.apache.http.entity.ContentType.APPLICATION_XML;
                            cType = "application/xml";
                            break;
                        default:
                            contentType = org.apache.http.entity.ContentType.APPLICATION_JSON;
                            cType = "application/json";
                    }
                }
                StringEntity entity = new StringEntity(payload, contentType);
                entity.setContentType(cType);
                requestBase.setEntity(entity);
            }
            if (params != null) {
                List<NameValuePair> urlParameters = new ArrayList<>();
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
                requestBase.setEntity(new UrlEncodedFormEntity(urlParameters));
            }
            HttpClient httpClient = HttpClientBuilder.create().build();
            return response(httpClient.execute(requestBase));
        } catch (IOException e) {
            throw new HttpException(e);
        }
    }

    public Response put(String url) throws HttpException {
        return send(new HttpPut(url));
    }

    public Response post(String url) throws HttpException {
        return send(new HttpPost(url));
    }

    public Response get(String url) throws HttpException {
        if (null == url)
            throw new IllegalArgumentException("Http URL is null");
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(url);
        try {
            //If it has params
            if (params != null && params.size() > 0) {
                URIBuilder uriBuilder = new URIBuilder(httpGet.getURI());
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    uriBuilder.addParameter(entry.getKey(), entry.getValue());
                }
                httpGet.setURI(uriBuilder.build());
            }
            httpGet.setHeader("User-Agent", Http.userAgent);
            if (headers != null) {
                for (Map.Entry<String, String> kv : headers.entrySet()) {
                    httpGet.setHeader(kv.getKey(), kv.getValue());
                }
            }
            HttpResponse httpResponse = httpClient.execute(httpGet);
            return response(httpResponse);
        } catch (IOException | URISyntaxException e) {
            throw new HttpException(e);
        }
    }

    private Response response(HttpResponse httpResponse) throws IOException {
        Response response = new Response();
        Map<String, String> headers = new HashMap<>();
        for (Header header : httpResponse.getAllHeaders()) {
            headers.put(header.getName(), header.getValue());
        }
        response.setHeaders(headers);
        response.setStatus(httpResponse.getStatusLine().getStatusCode());
        BufferedReader bufferedReader = null;
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8");
            bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder result = new StringBuilder();
            char[] buffer = new char[4048];
            int offset;
            while ((offset = bufferedReader.read(buffer)) != -1) {
                result.append(buffer, 0, offset);
            }
            response.setResponse(result);
        } finally {
            if (bufferedReader != null)
                bufferedReader.close();
            if (inputStreamReader != null)
                inputStreamReader.close();
        }
        return response;
    }

    public static class Response {
        private int status;
        private StringBuilder response;
        private Map<String, String> headers;

        public String getHeader(String key) {
            if (headers != null)
                return headers.get(key);
            else
                return null;
        }

        public Map<String, String> getHeaders() {
            return headers;
        }

        public void setHeaders(Map<String, String> headers) {
            this.headers = headers;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public StringBuilder getResponse() {
            return response;
        }

        public void setResponse(StringBuilder response) {
            this.response = response;
        }

    }
}

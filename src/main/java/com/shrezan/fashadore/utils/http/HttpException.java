package com.shrezan.fashadore.utils.http;

/**
 * Created by Dipak Malla
 * Email dpakmalla@gmail.com
 * Created on 11/21/18.
 */
public class HttpException extends Exception {
    public HttpException(Throwable throwable) {
        super(throwable);
    }
}
